# Data

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**city** | **string** |  | 
**coordinates** | [****](.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


