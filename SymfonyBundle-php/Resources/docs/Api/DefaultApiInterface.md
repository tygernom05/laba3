# Swagger\Server\Api\DefaultApiInterface

All URIs are relative to *https://virtserver.swaggerhub.com/Strateg192/RouteAndLamp2/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getData**](DefaultApiInterface.md#getData) | **POST** /GetData | Загружает данные с сервера
[**getRole**](DefaultApiInterface.md#getRole) | **POST** /GetRole | Возвращает роль пользователя
[**loadData**](DefaultApiInterface.md#loadData) | **POST** /LoadData | Загружает данные на сервера
[**login**](DefaultApiInterface.md#login) | **POST** /Login | Возвращает токен


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.default:
        class: Acme\MyBundle\Api\DefaultApi
        tags:
            - { name: "swagger_server.api", api: "default" }
    # ...
```

## **getData**
> getData($token, $city)

Загружает данные с сервера

Загружает данные с сервера

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class DefaultApi implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#getData
     */
    public function getData(Token $token = null, City $city = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | [**Swagger\Server\Model\Token**](../Model/Token.md)| token | [optional]
 **city** | [**Swagger\Server\Model\City**](../Model/City.md)| city | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getRole**
> getRole($token)

Возвращает роль пользователя

Возвращает роль пользователя

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class DefaultApi implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#getRole
     */
    public function getRole(Token $token = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | [**Swagger\Server\Model\Token**](../Model/Token.md)| token | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **loadData**
> loadData($token, $city, $data)

Загружает данные на сервера

Загружает данные на сервера

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class DefaultApi implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#loadData
     */
    public function loadData(Token $token = null, City $city = null, Data $data = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | [**Swagger\Server\Model\Token**](../Model/Token.md)| token | [optional]
 **city** | [**Swagger\Server\Model\City**](../Model/City.md)| city | [optional]
 **data** | [**Swagger\Server\Model\Data**](../Model/Data.md)| data | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **login**
> login($login, $password)

Возвращает токен

Возвращает токен

### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/DefaultApiInterface.php

namespace Acme\MyBundle\Api;

use Swagger\Server\Api\DefaultApiInterface;

class DefaultApi implements DefaultApiInterface
{

    // ...

    /**
     * Implementation of DefaultApiInterface#login
     */
    public function login(Login $login = null, Password $password = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**Swagger\Server\Model\Login**](../Model/Login.md)| login | [optional]
 **password** | [**Swagger\Server\Model\Password**](../Model/Password.md)| password | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

