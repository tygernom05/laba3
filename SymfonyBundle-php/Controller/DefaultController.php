<?php

/**
 * DefaultController
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Server\Controller
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Simple Inventory API
 *
 * This is a simple API
 *
 * OpenAPI spec version: 1.0.0
 * Contact: you@your-company.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Server\Controller;

use \Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Server\Api\DefaultApiInterface;
use Swagger\Server\Model\City;
use Swagger\Server\Model\Data;
use Swagger\Server\Model\Login;
use Swagger\Server\Model\Password;
use Swagger\Server\Model\Token;

/**
 * DefaultController Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Server\Controller
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class DefaultController extends Controller
{

    /**
     * Operation getData
     *
     * Загружает данные с сервера
     *
     * @param Request $request The Symfony request to handle.
     * @return Response The Symfony response.
     */
    public function getDataAction(Request $request)
    {
        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Figure out what data format to return to the client
        $produces = ['application/json'];
        // Figure out what the client accepts
        $clientAccepts = $request->headers->has('Accept')?$request->headers->get('Accept'):'*/*';
        $responseFormat = $this->getOutputFormat($clientAccepts, $produces);
        if ($responseFormat === null) {
            return new Response('', 406);
        }

        // Handle authentication

        // Read out all input parameter values into variables
        $token = $request->getContent();
        $city = $request->getContent();

        // Use the default value if no value was provided

        // Deserialize the input values that needs it
        $token = $this->deserialize($token, 'Swagger\Server\Model\Token', $inputFormat);
        $city = $this->deserialize($city, 'Swagger\Server\Model\City', $inputFormat);

        // Validate the input values
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\Token");
        $response = $this->validate($token, $asserts);
        if ($response instanceof Response) {
            return $response;
        }
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\City");
        $response = $this->validate($city, $asserts);
        if ($response instanceof Response) {
            return $response;
        }


        try {
            $handler = $this->getApiHandler();

            
            // Make the call to the business logic
            $responseCode = 204;
            $responseHeaders = [];
            $result = $handler->getData($token, $city, $responseCode, $responseHeaders);

            // Find default response message
            $message = 'ok';

            // Find a more specific message, if available
            switch ($responseCode) {
                case 201:
                    $message = 'ok';
                    break;
                case 400:
                    $message = 'invalid input, object invalid';
                    break;
                case schema:
                    $message = '';
                    break;
            }

            return new Response(
                $result?$this->serialize($result, $responseFormat):'',
                $responseCode,
                array_merge(
                    $responseHeaders,
                    [
                        'Content-Type' => $responseFormat,
                        'X-Swagger-Message' => $message
                    ]
                )
            );
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(500, 'An unsuspected error occurred.', $fallthrough));
        }
    }

    /**
     * Operation getRole
     *
     * Возвращает роль пользователя
     *
     * @param Request $request The Symfony request to handle.
     * @return Response The Symfony response.
     */
    public function getRoleAction(Request $request)
    {
        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Figure out what data format to return to the client
        $produces = ['application/json'];
        // Figure out what the client accepts
        $clientAccepts = $request->headers->has('Accept')?$request->headers->get('Accept'):'*/*';
        $responseFormat = $this->getOutputFormat($clientAccepts, $produces);
        if ($responseFormat === null) {
            return new Response('', 406);
        }

        // Handle authentication

        // Read out all input parameter values into variables
        $token = $request->getContent();

        // Use the default value if no value was provided

        // Deserialize the input values that needs it
        $token = $this->deserialize($token, 'Swagger\Server\Model\Token', $inputFormat);

        // Validate the input values
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\Token");
        $response = $this->validate($token, $asserts);
        if ($response instanceof Response) {
            return $response;
        }


        try {
            $handler = $this->getApiHandler();

            
            // Make the call to the business logic
            $responseCode = 204;
            $responseHeaders = [];
            $result = $handler->getRole($token, $responseCode, $responseHeaders);

            // Find default response message
            $message = 'ok';

            // Find a more specific message, if available
            switch ($responseCode) {
                case 201:
                    $message = 'ok';
                    break;
                case 400:
                    $message = 'invalid input, invalid token';
                    break;
                case schema:
                    $message = '';
                    break;
            }

            return new Response(
                $result?$this->serialize($result, $responseFormat):'',
                $responseCode,
                array_merge(
                    $responseHeaders,
                    [
                        'Content-Type' => $responseFormat,
                        'X-Swagger-Message' => $message
                    ]
                )
            );
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(500, 'An unsuspected error occurred.', $fallthrough));
        }
    }

    /**
     * Operation loadData
     *
     * Загружает данные на сервера
     *
     * @param Request $request The Symfony request to handle.
     * @return Response The Symfony response.
     */
    public function loadDataAction(Request $request)
    {
        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Figure out what data format to return to the client
        $produces = ['application/json'];
        // Figure out what the client accepts
        $clientAccepts = $request->headers->has('Accept')?$request->headers->get('Accept'):'*/*';
        $responseFormat = $this->getOutputFormat($clientAccepts, $produces);
        if ($responseFormat === null) {
            return new Response('', 406);
        }

        // Handle authentication

        // Read out all input parameter values into variables
        $token = $request->getContent();
        $city = $request->getContent();
        $data = $request->getContent();

        // Use the default value if no value was provided

        // Deserialize the input values that needs it
        $token = $this->deserialize($token, 'Swagger\Server\Model\Token', $inputFormat);
        $city = $this->deserialize($city, 'Swagger\Server\Model\City', $inputFormat);
        $data = $this->deserialize($data, 'Swagger\Server\Model\Data', $inputFormat);

        // Validate the input values
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\Token");
        $response = $this->validate($token, $asserts);
        if ($response instanceof Response) {
            return $response;
        }
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\City");
        $response = $this->validate($city, $asserts);
        if ($response instanceof Response) {
            return $response;
        }
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\Data");
        $response = $this->validate($data, $asserts);
        if ($response instanceof Response) {
            return $response;
        }


        try {
            $handler = $this->getApiHandler();

            
            // Make the call to the business logic
            $responseCode = 204;
            $responseHeaders = [];
            $result = $handler->loadData($token, $city, $data, $responseCode, $responseHeaders);

            // Find default response message
            $message = 'ok';

            // Find a more specific message, if available
            switch ($responseCode) {
                case 201:
                    $message = 'ok';
                    break;
                case 400:
                    $message = 'invalid input, object invalid';
                    break;
            }

            return new Response(
                $result?$this->serialize($result, $responseFormat):'',
                $responseCode,
                array_merge(
                    $responseHeaders,
                    [
                        'Content-Type' => $responseFormat,
                        'X-Swagger-Message' => $message
                    ]
                )
            );
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(500, 'An unsuspected error occurred.', $fallthrough));
        }
    }

    /**
     * Operation login
     *
     * Возвращает токен
     *
     * @param Request $request The Symfony request to handle.
     * @return Response The Symfony response.
     */
    public function loginAction(Request $request)
    {
        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Make sure that the client is providing something that we can consume
        $consumes = ['application/json'];
        $inputFormat = $request->headers->has('Content-Type')?$request->headers->get('Content-Type'):$consumes[0];
        if (!in_array($inputFormat, $consumes)) {
            // We can't consume the content that the client is sending us
            return new Response('', 415);
        }

        // Figure out what data format to return to the client
        $produces = ['application/json'];
        // Figure out what the client accepts
        $clientAccepts = $request->headers->has('Accept')?$request->headers->get('Accept'):'*/*';
        $responseFormat = $this->getOutputFormat($clientAccepts, $produces);
        if ($responseFormat === null) {
            return new Response('', 406);
        }

        // Handle authentication

        // Read out all input parameter values into variables
        $login = $request->getContent();
        $password = $request->getContent();

        // Use the default value if no value was provided

        // Deserialize the input values that needs it
        $login = $this->deserialize($login, 'Swagger\Server\Model\Login', $inputFormat);
        $password = $this->deserialize($password, 'Swagger\Server\Model\Password', $inputFormat);

        // Validate the input values
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\Login");
        $response = $this->validate($login, $asserts);
        if ($response instanceof Response) {
            return $response;
        }
        $asserts = [];
        $asserts[] = new Assert\Type("Swagger\Server\Model\Password");
        $response = $this->validate($password, $asserts);
        if ($response instanceof Response) {
            return $response;
        }


        try {
            $handler = $this->getApiHandler();

            
            // Make the call to the business logic
            $responseCode = 204;
            $responseHeaders = [];
            $result = $handler->login($login, $password, $responseCode, $responseHeaders);

            // Find default response message
            $message = 'ok';

            // Find a more specific message, if available
            switch ($responseCode) {
                case 201:
                    $message = 'ok';
                    break;
                case 400:
                    $message = 'invalid input, object invalid';
                    break;
                case schema:
                    $message = '';
                    break;
            }

            return new Response(
                $result?$this->serialize($result, $responseFormat):'',
                $responseCode,
                array_merge(
                    $responseHeaders,
                    [
                        'Content-Type' => $responseFormat,
                        'X-Swagger-Message' => $message
                    ]
                )
            );
        } catch (Exception $fallthrough) {
            return $this->createErrorResponse(new HttpException(500, 'An unsuspected error occurred.', $fallthrough));
        }
    }

    /**
     * Returns the handler for this API controller.
     * @return DefaultApiInterface
     */
    public function getApiHandler()
    {
        return $this->apiServer->getApiHandler('default');
    }
}
