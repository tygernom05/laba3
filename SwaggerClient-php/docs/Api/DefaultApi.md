# Swagger\Client\DefaultApi

All URIs are relative to *https://virtserver.swaggerhub.com/Strateg192/RouteAndLamp2/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getData**](DefaultApi.md#getData) | **POST** /GetData | Загружает данные с сервера
[**getRole**](DefaultApi.md#getRole) | **POST** /GetRole | Возвращает роль пользователя
[**loadData**](DefaultApi.md#loadData) | **POST** /LoadData | Загружает данные на сервера
[**login**](DefaultApi.md#login) | **POST** /Login | Возвращает токен


# **getData**
> getData($token, $city)

Загружает данные с сервера

Загружает данные с сервера

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = new \Swagger\Client\Model\Token(); // \Swagger\Client\Model\Token | token
$city = new \Swagger\Client\Model\City(); // \Swagger\Client\Model\City | city

try {
    $apiInstance->getData($token, $city);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | [**\Swagger\Client\Model\Token**](../Model/Token.md)| token | [optional]
 **city** | [**\Swagger\Client\Model\City**](../Model/City.md)| city | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRole**
> getRole($token)

Возвращает роль пользователя

Возвращает роль пользователя

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = new \Swagger\Client\Model\Token(); // \Swagger\Client\Model\Token | token

try {
    $apiInstance->getRole($token);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->getRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | [**\Swagger\Client\Model\Token**](../Model/Token.md)| token | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **loadData**
> loadData($token, $city, $data)

Загружает данные на сервера

Загружает данные на сервера

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$token = new \Swagger\Client\Model\Token(); // \Swagger\Client\Model\Token | token
$city = new \Swagger\Client\Model\City(); // \Swagger\Client\Model\City | city
$data = new \Swagger\Client\Model\Data(); // \Swagger\Client\Model\Data | data

try {
    $apiInstance->loadData($token, $city, $data);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->loadData: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | [**\Swagger\Client\Model\Token**](../Model/Token.md)| token | [optional]
 **city** | [**\Swagger\Client\Model\City**](../Model/City.md)| city | [optional]
 **data** | [**\Swagger\Client\Model\Data**](../Model/Data.md)| data | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **login**
> login($login, $password)

Возвращает токен

Возвращает токен

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\DefaultApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$login = new \Swagger\Client\Model\Login(); // \Swagger\Client\Model\Login | login
$password = new \Swagger\Client\Model\Password(); // \Swagger\Client\Model\Password | password

try {
    $apiInstance->login($login, $password);
} catch (Exception $e) {
    echo 'Exception when calling DefaultApi->login: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**\Swagger\Client\Model\Login**](../Model/Login.md)| login | [optional]
 **password** | [**\Swagger\Client\Model\Password**](../Model/Password.md)| password | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

